(define true #t)
(define false #f)

(define (runtime) (tms:clock (times)))

(define size 2)

(define pi 3.14159)

(define radius 72)

(define circumference (* pi (* 2 radius)))

(define (square x) (* x x))

(define (div a b)
  (floor (/ a b)))

(define (sum-of-square x y)
  (+ (square x) (square y)))

(define (fu a)
  (sum-of-square (+ 1 a) (+ 2 a)))

(define (cond-abs x)
  (cond ((> x 0) x)
        ((= x 0) x)
        ((< x 0) (- x))))

(define (if-abs x)
  (if (< x 0)
      (- x)
      x))

(define (average x y)
  (/ (+ x y) 2.0))

(define (improve guess x)
  (average guess (/ x guess)))

(define (good-enough? guess x)
  (< (if-abs (- (square guess) x)) 0.00000001))

(define (akar-iter guess x)
  (if (good-enough? guess x)
      guess
      (akar-iter (improve guess x) x)))

(define (akar x)
  (akar-iter 1.0 x))

(define (cube x)
  (* x x x))

(define (good-enough-kubus? guess x)
  (< (if-abs (- (cube guess) x)) 0.0000000001))

(define (improve-kubus guess x)
  (/ (+ (/ x
           (square guess))
        (* 2
           guess))
     3))

(define (akar-kubus-iter guess x)
  (if (good-enough-kubus? guess x)
      guess
      (akar-kubus-iter (improve-kubus guess x) x)))

(define (akar-kubus x)
  (akar-kubus-iter 1.0 x))

(define (factorial n)
  (define (iter product counter)
    (if (> counter n)
        product
        (iter (* counter product)
              (+ counter 1))))
  (iter 1 1))

(define (Ackermann x y)
  (cond ((= y 0) 0)
        ((= x 0) (* 2 y))
        ((= y 1) 2)
        (else (Ackermann (- x 1)
                         (Ackermann x
                                    (- y 1))))))

(define (fib n)
  (define (fib-iter a b count)
    (if (= count 0)
        b
        (fib-iter (+ a b) a (- count 1))))
  (fib-iter 1 0 n))

(define (count-change amount) (cc amount 5))
(define (cc amount kinds-of-coins)
  (cond ((= amount 0) 1)
        ((or (< amount 0) (= kinds-of-coins 0)) 0)
        (else (+ (cc amount
                     (- kinds-of-coins 1))
                 (cc (- amount
                        (first-denomination kinds-of-coins))
                     kinds-of-coins)))))
(define (first-denomination kinds-of-coins)
  (cond ((= kinds-of-coins 1) 1)
        ((= kinds-of-coins 2) 5)
        ((= kinds-of-coins 3) 10)
        ((= kinds-of-coins 4) 25)
        ((= kinds-of-coins 5) 50)))

;; exercise 1.11
(define (f-eleven n)
  (cond ((< 3 n) n)
        (else (+ (f-eleven (- n 1))
                 (* 2 (f-eleven (- n 2)))
                 (* 3 (f-eleven (- n 3)))))))

(define (sine angle)
  (define (p x)
    (- (* 3 x) (* 4 (cube x))))
  (if (not (> (abs angle) 0.1))
      angle
      (p (sine (/ angle 3.0)))))

(define (expt b n)
  (define (expt-iter b counter product)
    (if (= counter 0)
        product
        (expt-iter b
                   (- counter 1)
                   (* b product))))
  (expt-iter b n 1))

(define (fast-expt b n)
  (cond ((= n 0) 1)
        ((even? n) (square (fast-expt b (/ n 2))))
        (else (* b (fast-expt b (- n 1))))))

(define (another-fast-expt b n)
  (define (temp b n product)
    (cond ((= n 0) product)
          ((even? n) (temp (square b) (/ n 2) product))
          (else (temp b (- n 1) (* product b)))))
  (temp b n 1))

(define (fast-mult a b)
  (define (double a)
    (* a 2))
  (define (halve a)
    (/ a 2))
  (cond ((= b 0) 0)
        ((even? b) (double (fast-mult a (halve b))))
        (else (+ a (fast-mult a (- b 1))))))

(define (faster-mult a b)
  (define (f-mult a b product counter)
    (cond ((= a 0) product)
          ((= (remainder a 2) 1) (f-mult (div a 2)
                                         b
                                         (+ product (* (fast-expt 2 counter) b))
                                         (+ counter 1)))
          (else (f-mult (div a 2) b product (+ counter 1)))))
  (f-mult a b 0 0))

(define (fibo n)
  (define (fibo-iter a b p q count)
    (cond ((= count 0) b)
          ((even? count)
           (fibo-iter a
                      b
                      (+ (square p) (square q))
                      (+ (square q) (* 2 p q))
                      (div count 2)))
          (else (fibo-iter (+ (* b q) (* a q) (* a p))
                           (+ (* b p) (* a q))
                           p
                           q
                           (- count 1)))))
  (fibo-iter 1 0 0 1 n))

(define (euc-gcd a b)
  (if (= b 0)
      a
      (euc-gcd b (remainder a b))))

(define (divides? a b)
  (= (remainder b a) 0))

(define (smallest-divisor n)
  (define (find-divisor n test-divisor)
    (cond ((> (square test-divisor) n) n)
          ((divides? test-divisor n) test-divisor)
          (else (find-divisor n (+ test-divisor 1)))))
  (find-divisor n 2))

(define (prime? n)
  (= (smallest-divisor n) n))

(define (fermat-test n)
  (define (expmod base exp m)
    (cond ((= exp 0) 1)
          ((even? exp) (remainder (square (expmod base (/ exp 2) m)) m))
          (else (remainder (* base (expmod base (- exp 1) m)) m))))
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) true)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else false)))

(define (timed-prime-test n)
  (define (start-prime-test n start-time)
    (define (report-prime elapsed-time)
      (display "***")
      (newline)
      (display elapsed-time)
      (newline))
    (if (fermat-test n)
        (report-prime (- (runtime) start-time))))
  (display n)
  (newline)
  (start-prime-test n (runtime)))

(define (prime-check-odd? n)
  (define (next n)
    (if (= n 2)
        3
        (+ n 2)))
  (define (find-divisor n test-divisor)
    (cond ((> (square test-divisor) n) n)
          ((divides? test-divisor n) test-divisor)
          (else (find-divisor n (next test-divisor)))))
  (define (smallest-divisor n)
    (find-divisor n 2))
  (= n (smallest-divisor n)))

(define (sum-integers a b)
  (if (> a b)
      0
      (* a (sum-integers (+ a 1) (- b 1)))))

(define (sum-cubes a b)
  (if (> a b)
      0
      (+ (cube a)
         (sum-cubes (+ a 1) b))))

(define (pi-sum a b)
  (if (> a b)
      0
      (+ (/ 1.0 (* a (+ a 2)))
         (pi-sum (+ a 4) b))))

(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
         (sum term (next a) next b))))

(define (inc a) (+ a 1))

(define (sum-cubes-using-sum a b)
  (sum cube a inc b))

(define (identity x) x)

(define (sum-integers-using-sum a b)
  (sum identity a inc b))

(define (integral f a b dx)
  (define (add-dx x)
    (+ x dx))
  (* (sum f (+ a (/ dx 2.0)) add-dx b)
     dx))

(define (twice a) (* a 2))
(define (quad a) (* a 4))

(define (another-integral f a b n)
  (define (simpson-rule term a next b k n)
    (cond ((> a b) 0)
          ((or (= k 0) (= k n))
           (+ (term a)
              (simpson-rule term (next a) next b (+ k 1) n)))
          ((even? k)
           (+ (twice (term a))
              (simpson-rule term (next a) next b (+ k 1) n)))
          (else
           (+ (quad (term a))
              (simpson-rule term (next a) next b (+ k 1) n)))))
  (define h (/ (- b a) n))
  (define (next a) (+ a h))
  (* (/ h 3.0) (simpson-rule f a next b 0 n)))

(define (pi-sum a b)
  (sum (lambda (x) (/ 1.0 (* x (+ x 2))))
       a
       (lambda (x) (+ x 4))
       b))

(define (integral f a b dx)
  (* (sum f
          (+ a (/ dx 2.0))
          (lambda (x) (+ x dx))
          b)
     dx))

(define (f-let x y)
  (let ((a (+ 1 (* x y)))
        (b (- 1 y)))
    (+ (* x (square a))
       (* y b)
       (* a b))))

(define (close-enough? a b) (< (abs (- a b)) 0.001))

(define (search f neg-point pos-point)
  (let ((mid-point (average neg-point pos-point)))
    (if (close-enough? neg-point pos-point)
        mid-point
        (let ((test-value (f mid-point)))
          (cond ((positive? test-value)
                 search f neg-point mid-point)
                ((negative? test-value)
                 search f mid-point pos-point)
                (else mid-point))))))

(define (half-interval-method f a b)
  (let ((a-value (f a))
        (b-value (f b)))
    (cond ((and (negative? a-value) (positive? b-value))
           (search f a b))
          ((and (negative? b-value) (positive? a-value))
           (search f b a))
          (else
           (error "values are not of opposite sign" a b)))))

(half-interval-method (lambda (x) (- (* x x x) (* 2 x) 3))
                      1.0
                      2.0)

(define tolerance 0.000001)
(define (fixed-point f first-guess)
  (define (close-enough? a b)
    (< (abs (- a b))
       tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(define (sqrt-w-fixed-point x)
  (fixed-point (lambda (y) (/ x y))
               1.0))
